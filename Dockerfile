FROM rootproject/root:6.22.08-ubuntu20.04

# Change to root user for installation
USER root
ENV HOME /root

# Install dependencies
RUN apt-get update
RUN apt-get install -y --no-install-recommends \
    python3-setuptools python3-pip sudo \
  && pip3 install jupyterlab metakernel \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/*

# Set language stuff
RUN echo "en_US.UTF-8 UTF-8" > /etc/locale.gen && \
    locale-gen

# Create a user called "fp_student"
ENV SHELL=/bin/bash \
    NB_USER=fp_student \
    NB_UID=1000 \
    LC_ALL=en_US.UTF-8 \
    LANG=en_US.UTF-8 \
    LANGUAGE=en_US.UTF-8

# Add the user to the system
RUN useradd -m -s /bin/bash -u $NB_UID $NB_USER

USER $NB_UID
ENV HOME /home/$NB_USER
RUN mkdir /home/$NB_USER/work
RUN mkdir /home/$NB_USER/.jupyter
RUN jupyter notebook --generate-config
RUN echo "c.NotebookApp.extra_static_paths = [\"$ROOTSYS/js/\"]" >> /home/$NB_USER/.jupyter/jupyter_notebook_config.py

# Set the default directory for the container to /home/fp_student/work
WORKDIR /home/$NB_USER/work

# Install python packages for FP
RUN pip3 install \
    atlasify==0.4.2 \
    pandas==1.2.0 \
    pylorentz==0.3.3 \
    uproot==4.0.6 \
    matplotlib==3.3.3 \
    ipywidgets==7.6.3 \
    scipy==1.6.0 \
    awkward==1.1.2 \
    numba==0.53.1

# Expose port 8888 --> this way it doesn't have to be done in the command when 
# starting a container
# EXPOSE 8888
# By default, ROOT uses port 8888 for jupyter notebooks 
# If you wanna run on another port, use root --notebook --ip=0.0.0.0 --port <your_port>
CMD ["root", "--notebook", "--ip=0.0.0.0"]
